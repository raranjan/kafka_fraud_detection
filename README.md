# README #

##### Reference
https://florimond.dev/blog/articles/2018/09/building-a-streaming-fraud-detection-system-with-kafka-and-python/

### Network
#### Start
docker network create kafka-network
#### Remove
docker network rm kafka-network

### Kafka Broker
#### Start
docker-compose -f docker-compose.kafka.yaml up
#### Stop
docker-compose -f docker-compose.kafka.yaml down [or stop]

#### Check if Kafka Broker started
docker-compose -f docker-compose.kafka.yaml logs broker | grep "started"
#### Check logs for Kafka Broker
docker-compose -f docker-compose.kafka.yaml logs broker
#### Check the message in Kafka Topic
docker-compose -f docker-compose.kafka.yaml exec broker kafka-console-consumer --bootstrap-server localhost:9092 --topic queueing.transactions --from-beginning

### Start Generator and Producer
docker-compose up